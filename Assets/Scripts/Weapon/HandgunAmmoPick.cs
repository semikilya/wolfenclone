using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandgunAmmoPick : MonoBehaviour
{
    public GameObject fakeAmmoClip;
    public AudioSource ammoPickupSound;
    public GameObject pickUpDisplay;

    private void OnTriggerEnter(Collider other)
    {
        fakeAmmoClip.SetActive(false);
        ammoPickupSound.Play();
        GlobalAmmo.HandgunAmmo += 12;
        pickUpDisplay.SetActive(false);
        pickUpDisplay.GetComponent<Text>().text = "You get AMMO";
        pickUpDisplay.SetActive(true);
    }
}
