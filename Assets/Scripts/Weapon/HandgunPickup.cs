using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;

public class HandgunPickup : MonoBehaviour
{
    public GameObject realHandgun;
    public GameObject fakeHandgun;
    public AudioSource handunPickupSound;
    public GameObject handgunIcon; 
    public GameObject pickUpDisplay;

    private void OnTriggerEnter(Collider other)
    {
        realHandgun.SetActive(true);
        handgunIcon.SetActive(true);
        fakeHandgun.SetActive(false);
        handunPickupSound.Play();
        GetComponent<BoxCollider>().enabled = false;
        pickUpDisplay.SetActive(false);
        pickUpDisplay.GetComponent<Text>().text = "You get GUN";
        pickUpDisplay.SetActive(true);
    }
}
