using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GlobalAmmo : MonoBehaviour
{
    public static int HandgunAmmo;
    public GameObject ammoDisplay;
   
    void Update()
    {
        ammoDisplay.GetComponent<Text>().text = "" + HandgunAmmo;
    }
}
